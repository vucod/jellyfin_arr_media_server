#!/bin/bash

SCRIPT_DIR=$(dirname "$0")
SERVER_DIR="$SCRIPT_DIR/../"
SERVER_NAME=$(basename $(readlink -f $SERVER_DIR))

export DEBIAN_FRONTEND=noninteractive

# Apt
sudo apt-get update -y
sudo apt-get install -y python3.10-venv python3-pip

# Docker: https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc
sudo apt-get install -y ca-certificates curl  # Docker
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Cockpit: https://cockpit-project.org/running.html#ubuntu
. /etc/os-release
sudo apt-get install -y -t ${VERSION_CODENAME}-backports cockpit

# Jellyfin-mpv-shim
sudo apt-get -y install mpv  # NOTE mpv is not in the base image
sudo pip3 install --upgrade jellyfin-mpv-shim
sudo apt-get -y install python3-tk
sudo apt-get -y install python3-jinja2 python3-webview

# Glances dependency
sudo pip3 install --upgrade glances ensurepath bottle

# fintube
python3 -m pip install -U yt-dlp[default]
sudo apt-get -y install id3v2
clone_repo="https://github.com/AECX/FinTube.git"
clone_dir="$SERVER_DIR/configs/jellyfin/config/plugins/fintube"
if [ ! -d "$clone_dir" ]; then
    git clone "$clone_repo" "$clone_dir"
else
    echo "Directory $clone_dir already exists."
fi

# set mDNS: 
echo "Setting server name to: $SERVER_NAME.local"
sudo hostnamectl set-hostname $SERVER_NAME
sudo sed -i "s/^.*domain-name=.*/domain-name=local/" /etc/avahi/avahi-daemon.conf
sudo service avahi-daemon restart

# Change firewall rules
if ! grep -q 'iptables -t nat -I OUTPUT -p tcp -d 127.0.0.1 --dport 80 -j REDIRECT --to-ports 3000' /etc/rc.local || ! grep -q 'iptables -t nat -I PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 3000' /etc/rc.local; then
    # Add firewall rules
    sudo sed -i '/^exit 0/i \
    iptables -t nat -I OUTPUT -p tcp -d 127.0.0.1 --dport 80 -j REDIRECT --to-ports 3000\n\
    iptables -t nat -I PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 3000' /etc/rc.local
fi

# Python
pip install pipx

# Change font of hdmi output 
#sudo sh -c 'script -f /dev/tty1 && setfont /usr/share/consolefonts/Lat38-Terminus32x16.psf.gz'
sudo setfont /usr/share/consolefonts/Lat38-Terminus32x16.psf.gz
