#!/bin/bash
#
SCRIPT_DIR=$(dirname "$0")
SERVER_DIR="$SCRIPT_DIR/../"

mkdir -p $SERVER_DIR/data/media
mkdir -p $SERVER_DIR/data/media/movies
mkdir -p $SERVER_DIR/data/media/series
mkdir -p $SERVER_DIR/data/media/music
mkdir -p $SERVER_DIR/data/media/videos
mkdir -p $SERVER_DIR/data/downloads
mkdir -p ~/.config/jellyfin-mpv-shim/

sudo cp $SCRIPT_DIR/boot.sh /etc/init.d/jellyfin-mediarr-server.sh
sudo cp $SERVER_DIR/services/* /etc/systemd/system/
sudo cp $SERVER_DIR/configs/mpv/mpv.config ~/.config/jellyfin-mpv-shim/

$SCRIPT_DIR/substitute.sh
