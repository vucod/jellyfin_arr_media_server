#!/bin/bash
echo "Reloading systemctl"
sudo systemctl daemon-reload

echo "Reloading Jellyfin mpv shim"
sleep 5
sudo systemctl enable jellyfin-mpv-shim.service
sleep 5
sudo systemctl restart jellyfin-mpv-shim.service

echo "Reloading glances"
sudo systemctl enable glances.service
sudo systemctl restart glances.service
