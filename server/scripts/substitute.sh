#!/bin/bash
#
SCRIPT_DIR=$(dirname "$0")
SERVER_DIR="$SCRIPT_DIR/../"
export $(grep -v '^#' "$SERVER_DIR/.env" | xargs)

# replace variables in configuration
files=(
  "$SERVER_DIR/configs/bazarr/config/config.yaml"
  "$SERVER_DIR/configs/flemmarr/config.yml"
  "$SERVER_DIR/configs/jellyseerr/settings.json"
  "$SERVER_DIR/configs/lidarr/config.xml"
  "$SERVER_DIR/configs/prowlarr/config.xml"
  "$SERVER_DIR/configs/radarr/config.xml"
  "$SERVER_DIR/configs/readarr/config.xml"
  "$SERVER_DIR/configs/sonarr/config.xml"
)

for file in "${files[@]}"; do
    envsubst < "$file" > "$file.tmp" && mv "$file.tmp" "$file"
    echo "Updated file: $file with environement variables"
done
