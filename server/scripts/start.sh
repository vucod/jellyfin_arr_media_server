#!/bin/bash

SCRIPT_DIR=$(dirname "$0")
SERVER_DIR="$SCRIPT_DIR/../"

cd $SERVER_DIR
sudo docker compose up -d --remove-orphans
