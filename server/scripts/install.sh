#!/bin/bash
#
#
SCRIPT_DIR=$(dirname "$0")

$SCRIPT_DIR/move.sh             # Run once
$SCRIPT_DIR/configure.sh        # Run once
$SCRIPT_DIR/boot.sh             # Run once per boot
$SCRIPT_DIR/update.sh           # Run regularly
$SCRIPT_DIR/start.sh            # Run always
