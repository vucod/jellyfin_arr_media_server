#!/bin/bash
# apt
sudo apt -y update
sudo apt -y upgrade
sudo apt -y autoremove
sudo apt install deborphan
deborphan | xargs sudo apt-get -y remove --purge
sudo apt -y clean

# Logs
sudo find /var/log -type f -mtime +7 -exec rm {} \;

# Tmp
sudo rm -rf /tmp/*

# Cleanup docker
sudo docker stop $(sudo docker ps -aq);sudo docker rm $(sudo docker ps -aq)  

# docker
sudo docker system prune --volumes
