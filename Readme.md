# Media server based on arr apps and Jellyfin

Media server based on arr apps and Jellyfin.

## Running 

To run the server, copy the .env.template file to server/.env and configure it for your usage.
Then do:

    cd server/
    ./scripts/substitute.sh
    sudo docker compose up -d
 

## Installation on Orange Pi 5

The media server was assembled for Orange pi 5. It uses [Ubuntu-rockchip image](https://github.com/Joshua-Riek/ubuntu-rockchip).
To install it on that device with that base image, prepare a SD card with that base image. Make sure the device has access to a lot of storage. 
Boot the device and configure your system.
Copy the .env.template file to .env and configure it for your usage.
Make sure you can access the server over ssh.
Then launch:

    export $(cat .env | grep -v '^#' | xargs)
    ./scripts/install.sh
