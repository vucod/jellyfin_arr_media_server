#!/bin/bash
# Install jellyfin mediarr server into device

: "${SSH_USERNAME:=ubuntu}"
: "${SSH_HOST:=192.168.1.2}"
: "${SERVER_NAME:=servarr}"
: "${SERVER_DIR:=~/servarr}"

SCRIPT_DIR=$(dirname "$0")


echo "Copying content and install in: $SSH_USERNAME@$SSH_HOST"

scp -r "$SCRIPT_DIR/../server/"* "$SSH_USERNAME@$SSH_HOST:$SERVER_DIR"
scp -r "$SCRIPT_DIR/../.env" "$SSH_USERNAME@$SSH_HOST:$SERVER_DIR/.env"

ssh "$SSH_USERNAME@$SSH_HOST" "source $SERVER_DIR/.env; $SERVER_DIR/scripts/install.sh"
